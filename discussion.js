//MONGODB AGGREGATION

db.course_bookings.insertMany([
    {
        "courseId" : "C001",
        "studentId": "S004",
        "isCompleted": true
    },
    {
        "courseId" : "C002",
        "studentId": "S001",
        "isCompleted": false
    },
    {
        "courseId" : "C001",
        "studentId": "S003",
        "isCompleted": true
    },
    {
        "courseId" : "C003",
        "studentId": "S002",
        "isCompleted": false
    },
    {
        "courseId" : "C001",
        "studentId": "S003",
        "isCompleted": true
    },
    {
        "courseId" : "C004",
        "studentId": "S004",
        "isCompleted": false
    },
    {
        "courseId" : "C002",
        "studentId": "S007",
        "isCompleted": true
    },
    {
        "courseId" : "C003",
        "studentId": "S005",
        "isCompleted": false
    },
    {
        "courseId" : "C001",
        "studentId": "S008",
        "isCompleted": true
    },
    {
        "courseId" : "C004",
        "studentId": "S0013",
        "isCompleted": false
    }
]);

//Aggregation allows us to retrieve a group of data based on specific conditions

//count all documents
db.course_bookings.aggregate([
	{
		$group: {_id: null, count: {$sum: 1}}
	}
]);

//count all documents, grouped by courseId
db.course_bookings.aggregate([
	{
		$group: {_id: "$courseId", count: {$sum: 1}}
	}
]);

//count all documents with isCompleted = true, grouped by courseId
db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$group: {_id: "$courseId", total: {$sum: 1}}
	}

]);

//get all documents with isCompleted = true, show studentId field only
db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$project: {"studentId": 1}
	}

]);

//get all documents with isCompleted = true, sort courseId in descending order then studentId in ascending order
db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$sort: {"courseId": -1, "studentId": 1}
	}

]);

db.orders.insertMany([
	{
		"customer_Id": "A123",
		"amount": 500,
		"status": "A"
	},
	{
		"customer_Id": "A123",
		"amount": 250,
		"status": "A"
	},
	{
		"customer_Id": "B212",
		"amount": 200,
		"status": "A"
	},
	{
		"customer_Id": "B212",
		"amount": 200,
		"status": "D"
	},
]);

//get all documents with status = A, grouped by customer_Id, get max value for amount field
db.orders.aggregate([
	{
		$match: {"status": "A"}
	},
	{
		$group: {_id: "$customer_Id", maxAmount: {$max: "$amount"}}
	}
]);

//get all documents with status = A, grouped by customer_Id, get min value for amount field
db.orders.aggregate([
	{
		$match: {"status": "A"}
	},
	{
		$group: {_id: "$customer_Id", minAmount: {$min: "$amount"}}
	}
]);

//get all documents with status = A, grouped by customer_Id, get average value for amount field
db.orders.aggregate([
	{
		$match: {"status": "A"}
	},
	{
		$group: {_id: "$customer_Id", avgAmount: {$avg: "$amount"}}
	}
]);